package server

// @version 0.1.0-dev

// @contact.name API Support

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"

	"gitlab.com/fraq/gurba-http/docs" // docs is generated by Swag CLI, you have to import it.
)

const VERSION = "0.1.0-poc"

type Config struct {
	Hostname string // hostname for Swagger documentation
	BindAddr string // Address to bind/listen on
	BindPort string // Port to listen on

}

func Run(c Config) {
	docs.SwaggerInfo.Version = VERSION
	docs.SwaggerInfo.Host = c.Hostname
	docs.SwaggerInfo.BasePath = "/api/v1"
	docs.SwaggerInfo.Schemes = []string{"http"}

	// Set defaults
	if c.BindAddr == "" {
		c.BindAddr = "127.0.0.1"
	}

	if c.BindPort == "" {
		c.BindPort = "8080"
	}

	log.Printf("Starting gurba-http version %s...", VERSION)

	// No naked routes. Group all the things.
	rtr := gin.Default()
	api := rtr.Group("/api")
	v1 := api.Group("/v1")

	// Trivial stuff
	rtr.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	v1.GET("/ping", ping)

	rtr.Run(c.BindAddr + ":" + c.BindPort)
}

// @Summary Ping the VFT server
// @Description Used for health checks
// @ID ping
// @Accept  json
// @Produce  json
// @Success 200 {object} model.Reply
// @Router /ping [get]
func ping(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"message": "pong",
	})
}
